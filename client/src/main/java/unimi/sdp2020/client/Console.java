package unimi.sdp2020.client;

public class Console {
    public synchronized void println(Object o) {
        System.out.println(o.toString());
        System.out.print("> ");
    }

    public synchronized void print() {
        System.out.print("> ");
    }
}
