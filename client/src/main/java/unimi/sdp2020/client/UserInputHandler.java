package unimi.sdp2020.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import unimi.sdp2020.client.models.Measurement;
import unimi.sdp2020.client.models.Sensor;
import unimi.sdp2020.client.models.SummarizedStatistic;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.net.ConnectException;
import java.util.*;
import java.util.stream.Collectors;

public class UserInputHandler extends Thread {

    private final Gson gson = new GsonBuilder().create();
    private final Console console;
    private final WebTarget endpoint;

    public UserInputHandler(Console console, String gatewayAddress) {
        this.console = console;
        this.endpoint = ClientBuilder.newClient().target(gatewayAddress);
    }

    @Override
    public void run() {
        Scanner sc = new Scanner(System.in);
        console.print();
        while(true){
            Scanner lineScanner = new Scanner(sc.nextLine());
            try {
                String s = lineScanner.next("[a-z]");
                switch (s){
                    case "q":
                        System.exit(0);
                    case "n":
                        console.println(getNodes());
                        break;
                    case "s":
                        console.println(getNStatistics(lineScanner.nextInt()));
                        break;
                    case "m":
                        console.println(getSummarizedStats(lineScanner.nextInt()));
                        break;
                    case "h":
                        console.println(help());
                        break;
                    default:
                        console.println("Comando non presente");
                        break;
                }
            } catch (NoSuchElementException e){
                console.println(help());
            } catch (ProcessingException e){
                console.println(String.format("Connessione con il server all'indirizzo %s fallita", endpoint.getUri()));
            }
        }
    }

    private String help() {
        return "q - for exit\n" +
                "n - stampa l'elenco di tutti i nodi presenti nella rete\n" +
                "s <n> - stampa le ultime <n> statistiche (con timestamp) di quartiere\n" +
                "m <n> - stampa la deviazione standard e la media dell ultime <n> statistiche prodotte dal quartiere\n" +
                "h - stampa questo elenco";
    }

    private String getSummarizedStats(int n) {
        Response response = endpoint.path(String.format("measurement/stats/%d",n)).request().get();
        SummarizedStatistic summarizedStatistic = gson.fromJson(response.readEntity(String.class), SummarizedStatistic.class);
        return summarizedStatistic.toString();
    }

    private String getNStatistics(int n) {
        Response response = endpoint.path(String.format("measurement/%d",n)).request().get();
        Measurement[] measurements = gson.fromJson(response.readEntity(String.class), Measurement[].class);
        if(measurements.length == 0){
            return "Nessuna misurazione presente";
        }
        return Arrays.stream(measurements)
                .map(Objects::toString)
                .collect(Collectors.joining("\n"));
    }

    private String getNodes() {
        Response response = endpoint.path("sensors-net").request().get();
        Sensor[] sensors = gson.fromJson(response.readEntity(String.class), Sensor[].class);
        if(sensors.length == 0){
            return "Nessun nodo presente nella rete";
        }
        return Arrays.stream(sensors)
                .map(Objects::toString)
                .collect(Collectors.joining("\n"));
    }
}
