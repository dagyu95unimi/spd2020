package unimi.sdp2020.client;

import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import unimi.sdp2020.client.models.Notification;
import unimi.sdp2020.client.models.PushNotificationGrpc;

import java.util.Iterator;

public class NotificationHandler extends Thread {

    private final String pushNotificationServerAddress;
    private final Console console;

    public NotificationHandler(Console console, String pushNotificationServerAddress){
        this.pushNotificationServerAddress = pushNotificationServerAddress;
        this.console = console;
    }
    
    @Override
    public void run() {
        ManagedChannel channel = ManagedChannelBuilder.forTarget(pushNotificationServerAddress)
                .usePlaintext()
                .build();
        try{
            Iterator<Notification> iterator = PushNotificationGrpc.newBlockingStub(channel).getNotification(Empty.newBuilder().build());
            iterator.forEachRemaining(notification -> {
                String s = String.format("\n\u001b[46;1m%s\u001b[0m",notification.getNotification());
                console.println(s);
            });
        } catch (StatusRuntimeException e){
            console.println(String.format("Connessione con il notification server all'indirizzo %s fallita", pushNotificationServerAddress));
        } finally {
            channel.shutdownNow();
        }
    }
}
