package unimi.sdp2020.client.models;

import java.util.Objects;

public class Sensor {
    public String id;
    public String ip;
    public Integer port;

    public Sensor(String id, String ip, Integer port) {
        this.id = id;
        this.ip = ip;
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sensor sensor = (Sensor) o;
        return Objects.equals(id, sensor.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return String.format("Sensor #%s active at %s:%d",id,ip,port);
    }
}
