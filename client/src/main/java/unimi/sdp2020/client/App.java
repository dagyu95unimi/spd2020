package unimi.sdp2020.client;

public class App {

    public static void main(String[] args) {
        try{

            String gatewayAddress = args[0];
            String pushNotificationServerAddress = args[1];

            Console console = new Console();
            NotificationHandler notificationHandler = new NotificationHandler(console,pushNotificationServerAddress);
            UserInputHandler userInputHandler = new UserInputHandler(console,gatewayAddress);

            notificationHandler.start();
            userInputHandler.start();

        } catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Usage <gatewayAddress> <pushNotificationServerAddress>");
        }

    }
}
