package unimi.sdp2020.gateway.handler;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import unimi.sdp2020.gateway.App;

import java.io.IOException;
import java.util.logging.Logger;

public class PushNotificationServer extends Thread {
    private final int port;
    private Server server;

    private static Logger logger = Logger.getLogger(App.class.getName());

    public PushNotificationServer(int port){
        this.port = port;
    }

    @Override
    public void run() {
        PushNotificationImpl pushNotification = PushNotificationImpl.getInstance();
        try {
            server = ServerBuilder.forPort(port)
                    .addService(pushNotification)
                    .build()
                    .start();
            logger.info(String.format("Notification server avviato alla porta %d",port));
            server.awaitTermination();
        } catch (IOException e) {
            logger.warning(String.format("Impossibile avviare il notification server alla porta %s",port));
        } catch (InterruptedException e) {
            logger.warning("Notification server interrotto!");
        }
    }

}
