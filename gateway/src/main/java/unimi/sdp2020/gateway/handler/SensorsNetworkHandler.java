package unimi.sdp2020.gateway.handler;

import unimi.sdp2020.gateway.models.Sensor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SensorsNetworkHandler {
    private static SensorsNetworkHandler instance;
    private List<Sensor> sensorList;

    public static SensorsNetworkHandler getInstance() {
        if(instance == null)
            instance = new SensorsNetworkHandler();
        return instance;
    }

    public SensorsNetworkHandler(){
        this.sensorList = new ArrayList<>();
    }

    public synchronized List<Sensor> add(Sensor s) {
        if(sensorList.contains(s)){
            return null;
        } else {
            sensorList.add(s);
            PushNotificationImpl.add(s);
            return sensorList;
        }
    }

    public synchronized void remove(String id) {
        sensorList = sensorList
                        .stream()
                        .filter(e -> !e.id.equals(id))
                        .collect(Collectors.toList());
        PushNotificationImpl.remove(id);
    }

    public synchronized List<Sensor> getSensorList() {
        return sensorList;
    }

    public synchronized Integer numberOfNodes() {
        return sensorList.size();
    }
}
