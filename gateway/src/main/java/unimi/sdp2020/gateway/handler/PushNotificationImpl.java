package unimi.sdp2020.gateway.handler;

import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import unimi.sdp2020.gateway.models.Measurement;
import unimi.sdp2020.gateway.models.Sensor;
import unimi.sdp2020.gateway.models.Notification;
import unimi.sdp2020.gateway.models.PushNotificationGrpc;

import java.util.ArrayList;
import java.util.List;

public class PushNotificationImpl extends PushNotificationGrpc.PushNotificationImplBase {
    private static PushNotificationImpl instance;
    private List<StreamObserver<Notification>> streamObserverList;

    private PushNotificationImpl(){
        streamObserverList = new ArrayList<>();
    }

    public static PushNotificationImpl getInstance() {
        if(instance == null)
            instance = new PushNotificationImpl();
        return instance;
    }

    public static void remove(String id) {
        Notification notification = Notification.newBuilder().setNotification(String.format("Rimosso sensore con id %s",id)).build();
        getInstance().sendNotification(notification);
    }

    public static void add(Sensor s) {
        Notification notification = Notification.newBuilder().setNotification(String.format("Aggiunto %s",s)).build();
        getInstance().sendNotification(notification);
    }

    public static void newMeasurement(Measurement m) {
        Notification notification = Notification.newBuilder().setNotification(String.format("Nuova misurazione: %s",m)).build();
        getInstance().sendNotification(notification);
    }

    private void sendNotification(Notification notification) {
        streamObserverList.forEach(s -> s.onNext(notification));
    }

    @Override
    public void getNotification(Empty request, StreamObserver<Notification> responseObserver) {
        this.streamObserverList.add(responseObserver);
    }
}
