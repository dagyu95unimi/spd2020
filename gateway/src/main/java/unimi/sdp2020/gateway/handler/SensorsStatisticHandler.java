package unimi.sdp2020.gateway.handler;

import unimi.sdp2020.gateway.models.Measurement;
import unimi.sdp2020.gateway.models.SummarizedStatistic;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class SensorsStatisticHandler {
    private static SensorsStatisticHandler instance;
    private LinkedList<Measurement> measurementList;

    private SensorsStatisticHandler(){
        measurementList = new LinkedList<>();
    }

    public static SensorsStatisticHandler getInstance(){
        if(instance == null)
            instance = new SensorsStatisticHandler();
        return instance;
    }

    public synchronized void addMeasurement(Measurement m) {
        measurementList.addFirst(m);
        PushNotificationImpl.newMeasurement(m);
    }

    public List<Measurement> getLastNMeasurements(Integer n) {
        return measurementList.stream().limit(n).collect(Collectors.toList());
    }

    public SummarizedStatistic getStatisticOfLastNMeasurements(Integer n) {
        return new SummarizedStatistic(measurementList.stream().limit(n).collect(Collectors.toList()));
    }
}
