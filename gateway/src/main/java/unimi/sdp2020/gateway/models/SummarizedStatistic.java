package unimi.sdp2020.gateway.models;

import java.util.List;
import java.util.Objects;

public class SummarizedStatistic {

    Double stDev;
    Double mean;

    public SummarizedStatistic(List<Measurement> collection){
        setMean(collection.stream().mapToDouble(Measurement::getValue).summaryStatistics().getAverage());
        Double sumDifferences = collection.stream().mapToDouble(e -> Math.pow(e.getValue() - getMean(),2)).sum();
        setStDev(Math.sqrt(sumDifferences / collection.size()));
    }

    public Double getStDev() {
        return stDev;
    }

    public void setStDev(Double stDev) {
        this.stDev = stDev;
    }

    public Double getMean() {
        return mean;
    }

    public void setMean(Double mean) {
        this.mean = mean;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SummarizedStatistic that = (SummarizedStatistic) o;
        return Objects.equals(getStDev(), that.getStDev()) &&
                Objects.equals(getMean(), that.getMean());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStDev(), getMean());
    }

    @Override
    public String toString() {
        return String.format("mean: %.2f\nsetDev: %.2f\n",getMean(),getStDev());
    }
}