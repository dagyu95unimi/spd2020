package unimi.sdp2020.gateway;

import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import unimi.sdp2020.gateway.handler.PushNotificationServer;

import javax.ws.rs.ProcessingException;
import java.net.URI;
import java.util.logging.Logger;

public class App {

    private static Logger logger = Logger.getLogger(App.class.getName());

    public static ResourceConfig create() {
        return new Gateway();
    }

    public static void main(String ...args){
        System.setProperty("java.util.logging.SimpleFormatter.format","\u001b[47m\u001b[30m[%1$tT.%1$tL]\u001b[0m [%4$s] %5$s %n");
        try {
            int portGateway = Integer.parseInt(args[0]);
            int portPushNotification = Integer.parseInt(args[1]);
            GrizzlyHttpServerFactory.createHttpServer(URI.create(String.format("http://0.0.0.0:%d",portGateway)), create());
            new PushNotificationServer(portPushNotification).start();
        } catch (NumberFormatException | IndexOutOfBoundsException e){
            logger.warning("Usage <portGateway> <portPushNotification>");
        } catch (ProcessingException e){
            logger.warning("Impossibile avviare il server");
        }
    }
}
