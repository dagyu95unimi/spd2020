package unimi.sdp2020.gateway;

import org.glassfish.jersey.server.ResourceConfig;
import unimi.sdp2020.gateway.helpers.GsonMessageBodyHandler;
import unimi.sdp2020.gateway.services.SensorsNetworkREST;
import unimi.sdp2020.gateway.services.SensorsStatisticREST;

public class Gateway extends ResourceConfig {

    public Gateway(){
        super(
            SensorsNetworkREST.class,
            SensorsStatisticREST.class
        );
        register(GsonMessageBodyHandler.class);
    }
}
