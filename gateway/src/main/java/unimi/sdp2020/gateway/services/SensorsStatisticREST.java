package unimi.sdp2020.gateway.services;

import unimi.sdp2020.gateway.handler.SensorsStatisticHandler;
import unimi.sdp2020.gateway.models.Measurement;
import unimi.sdp2020.gateway.models.SummarizedStatistic;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("measurement")
public class SensorsStatisticREST {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMeasurement(Measurement m){
        SensorsStatisticHandler.getInstance().addMeasurement(m);
        return Response.status(Response.Status.ACCEPTED).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{n}")
    public Response getNStatistic(@PathParam("n") Integer n){
        List<Measurement> measurementList = SensorsStatisticHandler.getInstance().getLastNMeasurements(n);
        return Response.status(Response.Status.ACCEPTED).entity(measurementList).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stats/{n}")
    public Response getLastNStatistic(@PathParam("n") Integer n){
        SummarizedStatistic statisticOfLastNMeasurements = SensorsStatisticHandler.getInstance().getStatisticOfLastNMeasurements(n);
        return Response.status(Response.Status.ACCEPTED).entity(statisticOfLastNMeasurements).build();
    }

}