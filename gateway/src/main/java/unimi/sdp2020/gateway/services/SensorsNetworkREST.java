package unimi.sdp2020.gateway.services;


import unimi.sdp2020.gateway.handler.SensorsNetworkHandler;
import unimi.sdp2020.gateway.models.Sensor;
import unimi.sdp2020.gateway.App;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Path("sensors-net")
public class SensorsNetworkREST {

    private static Logger logger = Logger.getLogger(App.class.getName());

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addSensor(Sensor s){
        logger.info(String.format("Adding %s", s));
        List<Sensor> res = SensorsNetworkHandler.getInstance().add(s);
        if(res == null){
            return Response.status(Response.Status.CONFLICT).build();
        } else {
            return Response.status(Response.Status.CREATED).entity(res).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") String id){
        logger.info(String.format("Removing sensor with id %s", id));
        SensorsNetworkHandler.getInstance().remove(id);
        return Response.status(Response.Status.ACCEPTED).build();
    }

    @GET
    @Path("nodes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response numberOfNodes(){
        Integer numberOfNodes = SensorsNetworkHandler.getInstance().numberOfNodes();
        return Response.status(Response.Status.ACCEPTED).entity(numberOfNodes).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSensorList(){
        List<Sensor> sensorList = SensorsNetworkHandler.getInstance().getSensorList();
        return Response.status(Response.Status.ACCEPTED).entity(sensorList).build();
    }
}
