package unimi.sdp2020.pm10_sensor.structures;

import unimi.sdp2020.pm10_sensor.simulators.Buffer;
import unimi.sdp2020.pm10_sensor.models.Measurement;

import java.util.LinkedList;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class SlidingWindow implements Buffer {

    private final int size;
    private final int skip;
    private final Consumer<Measurement> hookWhenLocalStatisticIsGenerated;
    LinkedList<Measurement> queue;
    LinkedList<Measurement> local_statistics;

    public SlidingWindow(int size, double overlap_factor, Consumer<Measurement> hookWhenLocalStatisticIsGenerated){
        assert size != 0;
        assert overlap_factor > 0 && overlap_factor < 1;
        queue = new LinkedList<>();
        local_statistics = new LinkedList<>();
        this.size = size;
        this.skip = (int) Math.ceil(size * overlap_factor);
        this.hookWhenLocalStatisticIsGenerated = hookWhenLocalStatisticIsGenerated;
    }

    @Override
    public synchronized void addMeasurement(Measurement m) {
        queue.addFirst(m);
        if(queue.size() == size){
            double average = getAverage();
            Measurement statistic = new Measurement(m.getId(), m.getType(), average, System.currentTimeMillis());
            if(hookWhenLocalStatisticIsGenerated != null)
                hookWhenLocalStatisticIsGenerated.accept(statistic);
            moveToNextWindow();
        }
    }

    private void moveToNextWindow() {
        IntStream.range(0,skip).forEach(e -> queue.removeLast());
    }

    private double getAverage() {
        //noinspection OptionalGetWithoutIsPresent
        return queue
                .stream()
                .mapToDouble(Measurement::getValue)
                .average()
                .getAsDouble();
    }

}
