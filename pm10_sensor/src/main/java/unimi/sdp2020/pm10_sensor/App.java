package unimi.sdp2020.pm10_sensor;

import unimi.sdp2020.pm10_sensor.handler.SensorController;
import unimi.sdp2020.pm10_sensor.handler.SensorServer;
import unimi.sdp2020.pm10_sensor.models.Sensor;

import java.util.logging.Logger;

public class App {

    private static Logger logger = Logger.getLogger(App.class.getName());
    public final static String BG_RED = "\u001b[41m";
    public final static String BG_GREEN = "\u001b[42m";
    public final static String BG_RESET = "\u001b[0m";

    public static void main(String[] args) {
        System.setProperty("java.util.logging.SimpleFormatter.format","\u001b[47m\u001b[30m[%1$tT.%1$tL]\u001b[0m [%4$s] %5$s %n");
        try{
            String id = args[0];
            String ip = args[1];
            int port = Integer.parseInt(args[2]);
            String gatewayAddress = args[3];

            Sensor sensor = new Sensor(id,ip,port);
            logger.info(String.format("Creazione %s", sensor));
            SensorController controller = new SensorController(gatewayAddress, sensor);
            SensorServer sensorServer = new SensorServer(port, controller);
            sensorServer.start();

        } catch (NumberFormatException | IndexOutOfBoundsException e){
            logger.warning("Usage <id> <ip> <port> <gatewayAddress> ");
        }



    }
}
