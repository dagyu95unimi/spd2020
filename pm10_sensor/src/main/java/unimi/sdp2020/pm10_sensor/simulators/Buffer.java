package unimi.sdp2020.pm10_sensor.simulators;

import unimi.sdp2020.pm10_sensor.models.Measurement;

public interface Buffer {

    void addMeasurement(Measurement m);

}
