package unimi.sdp2020.pm10_sensor.models;

import java.util.Objects;

public class Sensor implements Comparable<Sensor> {
    public String id;
    public String ip;
    public Integer port;

    public Sensor(String id, String ip, Integer port) {
        this.id = id;
        this.ip = ip;
        this.port = port;
    }

    public Sensor(SensorInfo request) {
        this.id = request.getId();
        this.ip = request.getIp();
        this.port = request.getPort();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sensor sensor = (Sensor) o;
        return Objects.equals(id, sensor.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return String.format("Sensor %s active at %s:%d",id,ip,port);
    }

    public String getAddress() {
        return String.format("%s:%d", ip, port);
    }

    public SensorInfo toSensorInfo() {
        return SensorInfo.newBuilder().setId(id).setIp(ip).setPort(port).build();
    }

    @Override
    public int compareTo(Sensor s) {
        return id.compareTo(s.id);
    }
}
