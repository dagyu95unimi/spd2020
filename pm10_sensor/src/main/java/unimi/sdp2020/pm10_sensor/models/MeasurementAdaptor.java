package unimi.sdp2020.pm10_sensor.models;

public class MeasurementAdaptor {
    public static MeasurementAvg toMeasurementAvg(Measurement measurement) {
        return  MeasurementAvg.newBuilder()
                .setId(measurement.getId())
                .setType(measurement.getType())
                .setValue(measurement.getValue())
                .setTimestamp(measurement.getTimestamp())
                .build();
    }
}
