package unimi.sdp2020.pm10_sensor.handler;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import unimi.sdp2020.pm10_sensor.App;

import javax.ws.rs.ProcessingException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Logger;

public class SensorServer extends Thread{
    private final int port;
    private final SensorNetworkHandlerImpl sensorNetworkHandler;
    private final SensorController controller;
    private Server server;
    private final static Logger logger = Logger.getLogger(App.class.getName());

    public SensorServer(int port, SensorController controller){
        this.port = port;
        this.sensorNetworkHandler = new SensorNetworkHandlerImpl(controller);
        this.controller = controller;
    }

    @Override
    public void run() {
        try {
            server = ServerBuilder.forPort(port)
                    .addService(sensorNetworkHandler)
                    .build()
                    .start();
            logger.info("Server del sensore avviato");
            boolean res = controller.sayHelloToGateway();
            if(!res){
                throw new IllegalStateException();
            }
            controller.start();

            new Thread(() -> {
                Scanner sc = new Scanner(System.in);
                sc.nextLine();
                SensorServer.this.interrupt();
            }).start();

            server.awaitTermination();
        } catch (IllegalStateException e) {
            logger.warning(String.format("Impossibile aggiungere il sensore %s",controller.getSensor()));
        } catch (InterruptedException e) {
            logger.info("Sensore interrotto");
        } catch (IOException e) {
            logger.warning(String.format("Impossibile avviare il server alla porta %d",port));
        } catch (ProcessingException e){
            logger.warning("Impossibile contattare il gateway");
        }
    }

    @Override
    public void interrupt() {
        if(server != null){
            server.shutdownNow();
        }
        // Creare una rete A-B-C e rimuovere il nodo B
        // non appena invia il token
//        logger.info("Server interrotto!! 5s in attesa");
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        if(controller != null){
            controller.interrupt();
        }
        super.interrupt();
    }



}
