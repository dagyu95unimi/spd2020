package unimi.sdp2020.pm10_sensor.handler;

import java.util.List;
import java.util.stream.Collectors;

public class ParallelExecution {

    private final List<Thread> runnableStream;
    private int threads;

    public ParallelExecution(List<Runnable> runnableStream) {
        this.runnableStream = initRunnable(runnableStream);
        this.threads = runnableStream.size();
    }

    private List<Thread> initRunnable(List<Runnable> runnableStream) {
        return runnableStream.stream()
            .map(r -> new Thread(() -> {
                    try{
                        r.run();
                    } catch (Exception e){
                        e.printStackTrace();
                    } finally {
                        this.terminate();
                    }
                }))
            .collect(Collectors.toList());
    }


    public synchronized void awaitTermination(long millis) {
        if(runnableStream.size() > 0){
            runnableStream.forEach(Thread::start);
            try {
                wait(millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            runnableStream.forEach(Thread::interrupt);
        }
    }

    private synchronized void terminate() {
        threads -= 1;
        if(threads == 0)
            notify();
    }

}
