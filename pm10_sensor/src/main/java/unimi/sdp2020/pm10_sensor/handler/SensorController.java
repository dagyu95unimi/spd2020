package unimi.sdp2020.pm10_sensor.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import unimi.sdp2020.pm10_sensor.App;
import unimi.sdp2020.pm10_sensor.models.*;
import unimi.sdp2020.pm10_sensor.simulators.PM10Simulator;
import unimi.sdp2020.pm10_sensor.structures.SlidingWindow;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class SensorController extends Thread {

    private List<Sensor> sensorList;
    private final String gatewayAddress;
    private final Sensor sensor;
    private Token token;
    private PM10Simulator pm10Simulator;

    private final static Logger logger = Logger.getLogger(App.class.getName());

    public SensorController(String gatewayAddress, Sensor sensor) {
        this.sensor = sensor;
        this.gatewayAddress = gatewayAddress;
    }

    @Override
    public void interrupt() {
        pm10Simulator.stopMeGently();
        notifyDeletionOfSensor(sensor);
        //se possiede il token allora viene inviato al prossimo
        sendTokenToNext();
        super.interrupt();
    }

    @Override
    public void run() {
        SlidingWindow sw = new SlidingWindow(12,0.5,this::updateMeasurement);
        pm10Simulator = new PM10Simulator(sw);
        sayHelloToOthersNode();
        logger.info("Sensore avviato");
        pm10Simulator.start();
    }

    public synchronized void updateToken(Token request) {
        printOnLogger("Token ricevuto",true);
        token = Token.newBuilder(request).build();
    }

    private void printOnLogger(String format, boolean hasToken) {
        logger.info(String.format("%s %s%s",hasToken ? App.BG_GREEN : App.BG_RED,App.BG_RESET,format));
    }

    public synchronized void removeSensor(SensorInfo request) {
        printOnLogger(String.format("Sensore rimosso: %s",request),token != null);
        sensorList.removeIf(s -> s.id.equals(request.getId()));
    }

    public synchronized void insertSensor(SensorInfo request) {
       printOnLogger(String.format("Sensore inserito: %s",request),token != null);
       System.out.println(this.sensorList);
       Sensor newSensor = new Sensor(request);
       // Ovviamente esistono soluzioni più efficienti per gestire liste ordinate
       // ma non è questo l'obiettivo di questo progetto
       sensorList.add(newSensor);
       Collections.sort(sensorList);
    }

    private synchronized void updateMeasurement(Measurement measurement) {
        if(token == null && sensorList.size() == 1){
            token = Token.newBuilder().build();
            printOnLogger("Generato un nuovo token",true);
        }

        measurement.setId(sensor.id);
        MeasurementAvg measurementAvg = MeasurementAdaptor.toMeasurementAvg(measurement);
        //se possiedo il token
        if(token != null){
          token = Token.newBuilder(token).addMeasurements(measurementAvg).build();
          boolean allStatisticsArePresent = token.getMeasurementsList().size() >= sensorList.size();
          if(allStatisticsArePresent){
              Measurement toSent = generateStatistic(measurement.getType());
              printOnLogger(String.format("Invio statistica globale al server: %s\nLista di origine:\n%s",toSent,token.getMeasurementsList()), true);
              sendMeasurementToGateway(toSent);
              token = Token.newBuilder().build();
          }
          sendTokenToNext();
        } else {
        printOnLogger("Misurazione persa",false);
        }
    }

    private Measurement generateStatistic(String type) {
        @SuppressWarnings("OptionalGetWithoutIsPresent")
        double average = token.getMeasurementsList()
                .stream()
                .mapToDouble(MeasurementAvg::getValue)
                .average()
                .getAsDouble();
        // Sarebbe più corretto inserire la mediana dei tempi piuttosto che il tempo corrente di invio??
        return new Measurement(sensor.id, type, average,System.currentTimeMillis());
    }

    private synchronized void sendTokenToNext() {
        // Se ho il token e il prossimo non sono io allora lo invio
        if(sensorList.size() > 1 && token != null){
            int nSensors = sensorList.size();
            int index = IntStream.range(0, nSensors)
                    .filter(i -> sensorList.get(i).equals(sensor))
                    .findFirst()
                    .orElse(-1);
            for(int i = ((index + 1) % nSensors); i != index; i = ((i + 1) % nSensors)) {
                Sensor next = sensorList.get(i);
                ManagedChannel channel = ManagedChannelBuilder.forTarget(next.getAddress())
                        .usePlaintext()
                        .build();
                try {
                    printOnLogger(String.format("Invio token a %s",next),true);
                    SensorNetworkHandlerGrpc.newBlockingStub(channel).sendToken(token);
                    //ho inviato il token lo annullo
                    token = null;
                    break;
                } catch (StatusRuntimeException e) {
                    // Se il sensore non risponde allora viene ignorato
                    logger.warning(String.format("%s non risponde",next));
                } finally {
                    channel.shutdownNow();
                }
            }

        }
    }

    private void removeSensor(Sensor sensor) {
        printOnLogger(String.format("Sensore rimosso: %s", sensor), token != null);
        sensorList.removeIf(s -> s.equals(sensor));
    }

    private synchronized void sayHelloToOthersNode() {

        // Quando viene visualizzato questo messaggio dal nodo x
        // allora rimuovere il nodo y con y!=x
//        logger.info("In attesa di presentarsi 10s");
//        try {
//            Thread.sleep(10000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        List<Runnable> runnableStream;
        runnableStream = sensorList.stream()
            .filter(s -> !s.equals(sensor))
            .map(s -> (Runnable) () -> {
                ManagedChannel channel = ManagedChannelBuilder.forTarget(s.getAddress())
                        .usePlaintext()
                        .build();
                try{
                    printOnLogger(String.format("Mi presento a %s", s.getAddress()), token != null);
                    SensorNetworkHandlerGrpc.newBlockingStub(channel).insertSensor(sensor.toSensorInfo());
                } catch (Exception e){
                    // Se il sensore non risponde allora viene rimosso dalla lista
                    removeSensor(s);
                } finally {
                    channel.shutdownNow();
                }
            })
            .collect(Collectors.toList());
        ParallelExecution executionPool = new ParallelExecution(runnableStream);
        executionPool.awaitTermination(60 * 1000);
        logger.info("Presentazione ai nodi completata");
    }


    private void sendMeasurementToGateway(Measurement measurement) {
        Gson gson = new GsonBuilder().create();
        ClientBuilder.newClient()
                .target(gatewayAddress)
                .path("measurement")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(gson.toJson(measurement)));
    }

    public synchronized boolean sayHelloToGateway() {
        Gson gson = new GsonBuilder().create();
        Response response = ClientBuilder.newClient()
            .target(gatewayAddress)
            .path("sensors-net")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.json(gson.toJson(sensor)));
        if(response.getStatus() == Response.Status.CREATED.getStatusCode()) {
            List<Sensor> list = Arrays.asList(gson.fromJson(response.readEntity(String.class), Sensor[].class));
            logger.info(String.format("Lista ricevuta dal gateway:\n%s",list));
            this.sensorList = new ArrayList<>(list);
            Collections.sort(sensorList);


            if(token == null && sensorList.size() == 1){
                token = Token.newBuilder().build();
                printOnLogger("Generato un nuovo token",true);
            }

            return true;
        }
        return false;
    }

    public void notifyDeletionOfSensor(Sensor sensor){

        ClientBuilder.newClient()
            .target(gatewayAddress)
            .path(String.format("sensors-net/%s",sensor.id))
            .request()
            .delete();


        List<Runnable> runnableStream;
        synchronized (this){
            runnableStream = sensorList.stream()
                .filter(s -> !s.equals(sensor))
                .map(s -> (Runnable) () -> {
                    printOnLogger(String.format("Send to %s", s.getAddress()),token != null);
                    ManagedChannel channel = ManagedChannelBuilder.forTarget(s.getAddress())
                            .usePlaintext()
                            .build();
                    try {
                        SensorNetworkHandlerGrpc.newBlockingStub(channel).removeSensor(sensor.toSensorInfo());
                    } catch (Exception e){
                        logger.warning(String.format("Impossibile inviare segnale di rimozione a %s",sensor));
                    } finally {
                        channel.shutdownNow();
                    }
                })
                .collect(Collectors.toList());
        }
        ParallelExecution executionPool = new ParallelExecution(runnableStream);
        executionPool.awaitTermination(60 * 1000);

    }

    public Sensor getSensor() {
        return sensor;
    }
}
