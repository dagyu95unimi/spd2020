package unimi.sdp2020.pm10_sensor.handler;

import com.google.protobuf.Empty;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import unimi.sdp2020.pm10_sensor.models.SensorInfo;
import unimi.sdp2020.pm10_sensor.models.SensorNetworkHandlerGrpc;
import unimi.sdp2020.pm10_sensor.models.Token;

public class SensorNetworkHandlerImpl extends SensorNetworkHandlerGrpc.SensorNetworkHandlerImplBase {

    private SensorController controller;

    public SensorNetworkHandlerImpl(SensorController controller) {
        this.controller = controller;
    }

    @Override
    public void sendToken(Token request, StreamObserver<Empty> responseObserver) {
        try {
            controller.updateToken(request);
            responseObserver.onNext(Empty.newBuilder().build());
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL));
        }
    }

    @Override
    public void removeSensor(SensorInfo request, StreamObserver<Empty> responseObserver) {
        try {
            controller.removeSensor(request);
            responseObserver.onNext(Empty.newBuilder().build());
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL));
        }
    }

    @Override
    public void insertSensor(SensorInfo request, StreamObserver<Empty> responseObserver) {
        try {
            controller.insertSensor(request);
            responseObserver.onNext(Empty.newBuilder().build());
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL));
        }
    }
}