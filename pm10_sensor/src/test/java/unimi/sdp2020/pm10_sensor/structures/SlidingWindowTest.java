package unimi.sdp2020.pm10_sensor.structures;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import unimi.sdp2020.pm10_sensor.models.Measurement;

public class SlidingWindowTest {

    @Test
    public void avgIsRight(){
        SoftAssertions softAssertions = new SoftAssertions();
        final Measurement[] statistic = new Measurement[1];
        SlidingWindow sw = new SlidingWindow(4,0.5, (t) -> statistic[0] = t);
        sw.addMeasurement(new Measurement("1","pm10",2, System.currentTimeMillis()));
        sw.addMeasurement(new Measurement("1","pm10",2, System.currentTimeMillis()));
        sw.addMeasurement(new Measurement("1","pm10",4, System.currentTimeMillis()));
        softAssertions.assertThat(statistic[0]).isNull();
        sw.addMeasurement(new Measurement("1","pm10",4, System.currentTimeMillis()));
        softAssertions.assertThat(statistic[0].getValue()).isEqualTo(3.0);
        softAssertions.assertThat(sw.queue).hasSize(2);
        softAssertions.assertAll();
    }
}
