# Progetto di Sistemi Distribuiti e Pervasivi AA 2019/2020
- Autore: Gaetano D'Agostino
- Matricola: 939866
- Data esame: 22 Settembre 2020
- Github URL: https://gitlab.com/dagyu95unimi/spd2020

# Table of contents

<!--ts-->
   * [Componenti del sistema distribuito](#componenti-del-sistema-distribuito)
      * [Gateway](#gateway)
         * [REST server](#rest-server)
         * [Push notification server](#push-notification-server)
      * [Client analista](#client-analista)
      * [Nodo](#nodo)
   * [Scambio di messaggi](#scambio-di-messaggi)
      * [Aggiunta di un nuovo nodo alla rete](#aggiunta-di-un-nuovo-nodo-alla-rete)
      * [Rimozione di un nodo dalla rete](#rimozione-di-un-nodo-dalla-rete)
      * [Invio del token](#invio-del-token)
   * [Casi limite](#casi-limite)
      * [Due nodi entrano contemporanemente nella rete](#due-nodi-entrano-contemporanemente-nella-rete)
      * [Due nodi escono contemporaneamente dalla rete](#due-nodi-escono-contemporaneamente-dalla-rete)
      * [Un nodo si sta rimuovendo e un altro sta per inviare il token](#un-nodo-si-sta-rimuovendo-e-un-altro-sta-per-inviare-il-token)
      * [Un nodo si sta presentando e un altro sta per inviare il token](#un-nodo-si-sta-presentando-e-un-altro-sta-per-inviare-il-token)
      * [Un nodo si sta presentando e un altro sta uscendo](#un-nodo-si-sta-presentando-e-un-altro-sta-uscendo)
   * [Alcune note implementative](#alcune-note-implementative)

<!-- Added by: dagyu95, at: dom 20 set 2020, 13.25.54, CEST -->

<!--te-->

# Componenti del sistema distribuito

![](./img/architecture.png)

Come da figura i componenti del sistema distribuito sono: *i nodi della rete*, un *gateway* e un *client analista*. Il codice relativo
ad ogni componente è stato organizzato in una apposita cartella separata e indipendente dalle altre:

- `gateway/`: contiene il codice relativo al gateway
- `pm10_sensor/`: contiene il codice relativo ad un generico nodo della rete
- `client/`: contiene il codice al client analista

## Gateway
Per avviare il gateway:
```shell script
cd gateway
gradle run --args "<gatewayPort> <pushNotificationServerPort>" --console=plain --quiet
```

* `gatewayPort` la porta del server REST del gateway
* `pushNotificationServerPort` la porta del server gRPC che si occupa di inviare le push
notification

### REST server
Il gateway fornisce i seguenti servizi REST all'indirizzo di base `http://localhost:<gatewayPort>`:

| Path                      | Metodo HTTP   | Input         | Output                | Info                                                                              |
| ---------                 | -----------   | ----          | --------              | ----                                                                              |
| `sensors-net`             | GET           |               | Sensor[]              | Restituisce la lista dei nodi presenti nella rete                                 |   
| `sensors-net`             | POST          | Sensor        | Sensor[]              | Aggiunge il sensore alla lista dei nodi e restituisce la lista dei nodi presenti  |
| `sensors-net/{id}`        | DELETE        |               |                       | Rimuove il sensore con id {id}                                                    |
| `sensor-net/nodes`        | GET           |               | Integer               | Restituisce il numero di nodi presenti nella rete                                 |         
| `measurement`             | POST          | Measurement   |                       | Aggiunge una nuova misurazione                                                    |
| `measurement/{n}`         | GET           |               | Measurement[]         | Restituisce la lista delle ultime {n} misurazioni                                 |
| `measurement/stats/{n}`   | GET           |               | SummarizedStatistic   | Restituisce la media e la deviazione standard delle ultime {n} misurazioni        |

### Push notification server
Il gateway fornisce un servizio gRPC all'indirizzo target `localhost:<pushNotificationServerPort>` che permette 
a chi ne fa richiesta (tramite RPC) di ricevere uno stream di `Notification`

```protobuf
syntax = "proto3";

import "google/protobuf/empty.proto";

option java_multiple_files = true;
option java_package = "unimi.sdp2020.gateway.models";
option  java_outer_classname = "PushNotificationProto";
option objc_class_prefix = "HLW";

package push_notification_server;

service PushNotification {
  rpc getNotification (google.protobuf.Empty) returns (stream Notification) {}
}

message Notification {
  string notification = 1;
}
```

## Client analista
Per avviare il client analista:
```shell script
cd client
gradle run --args "<gatewayAddress> <pushNotificationServerAddress>" --console=plain --quiet
```

* `gatewayAddress`: l'URL del server REST del gateway
* `pushNotificationServerAddress`: l'URL del server gRPC che si occupa di inviare le push
notification


Il client analista è un'interfaccia a linea di comando che si occupa di interagire con l'interfaccia REST. 
Ecco l'elenco dei possibili comandi:
```
q - for exit
n - stampa l'elenco di tutti i nodi presenti nella rete
s <n> - stampa le ultime <n> statistiche (con timestamp) di quartiere
m <n> - stampa la deviazione standard e la media dell ultime <n> statistiche prodotte dal quartiere
h - stampa questo elenco
```

Inoltre tramite stream riceve delle **push notification** dal gateway. 

## Nodo
Per avviare un nodo:
```shell script
cd pm10_sensor
gradle run --args "<id> <ip> <port> <gatewayAddress>" --console=plain --quiet
```

* `id`: id del Sensore
* `ip`: indirizzo ip del sensore
* `port`: porta del sensore
* `gatewayAddress`: l'URL del server REST del gateway


Ogni sensore (nodo della rete) espone il seguente servizio gRPC:
```protobuf
syntax = "proto3";

import "google/protobuf/empty.proto";

option java_multiple_files = true;
option java_package = "unimi.sdp2020.pm10_sensor.models";
option  java_outer_classname = "SensorNetworkHandlerProto";
option objc_class_prefix = "HLW";

package pm10_sensor;

service SensorNetworkHandler {
  rpc sendToken (Token) returns (google.protobuf.Empty) {}
  rpc removeSensor (SensorInfo) returns (google.protobuf.Empty) {}
  rpc insertSensor (SensorInfo) returns (google.protobuf.Empty) {}
}

message Token {
  repeated MeasurementAvg measurements = 1;
}

message MeasurementAvg {
  string id = 1;
  string type = 2;
  double value = 3;
  uint64 timestamp = 4;
}

message SensorInfo {
  string id = 1;
  string ip = 2;
  int32 port = 3;
}
```

nello specifico:
* `sendToken`: invia il token al sensore
* `removeSensor`: comunica al sensore che il sensore `SensorInfo` è stato rimosso dalla rete
* `insertSensor`: comunica al sensore che il sensore `SensorInfo` si è aggiunto alla rete

# Scambio di messaggi
## Aggiunta di un nuovo nodo alla rete

![](./img/insertion.png)

1. Viene effettuata una POST al gateway al path `sensors-net` con i dettagli del nodo (sensore) che si vuole aggiungere. Il gateway risponde
    - 409: se esiste già un sensore con lo stesso `id`
    - 202: se l'inserimento è andato a buon fine
1. Viene inviato un messaggio broadcast in parallelo alla rete dei nodi tramite la RPC `insertSensor` che comunica 
l'inserimento del sensore. Se avviene qualche errore nell'esecuzione dell'RPC allora il nodo che riporta il fallimento viene rimosso
dalla lista

## Rimozione di un nodo dalla rete

Per segnalare la rimozione di un nodo è necessario dare in un input al nodo un qualsiasi carattere seguito da ENTER o soltanto ENTER.

![](./img/deletion.png)

1. Viene effettuata una DELETE al gateway al path `sensors-net/{id}` dove `{id}` è l'id del nodo che si vuole rimuovere. Il gateway risponde con 202 anche qualora non esiste un nodo con quell'id
1. Viene inviato un messaggio broadcast in parallelo alla rete dei nodi tramite la RPC `removeSensor` che comunica
la rimozione del sensore. Se avviene qualche errore nell'esecuzione dell'RPC allora non viene fatto nulla ma soltanto stampato nei log
il nodo che ha riportato l'errore. Questo non crea problemi perché il nodo che non ha ricevuto il segnale di rimozione
quando si accorgerà che il nodo rimosso non é più raggiungibile allora effettuerà la rimozione dello stesso dalla lista.

**N.B.** In fase di rimozione bisogna annotare questi due comportamenti:
* Prima di inviare il messagio 1 al gateway il sensore "spegne" il server gRPC e quindi non è più raggiunginbile. Si potrebbe
spegnere il server successivamente al broadcast della rete ma ho scelto questa implementazione per evitare che 
arrivassero eventuali token in fase di rimozione, in tal caso non succede nulla di grave (lo posso dimostrare) perché
il token viene rispedito ma viene inserita una misurazione dopo la rimozione del sensore; ovviamente si tratta di scelte
implementative di cui si può discutere e che sono entrambe supportate.
* Nel caso in cui il nodo che si sta rimuovendo possiede il token allora poco prima di terminare il processo (subito dopo 
il broadcast alla rete) lo spedisce al nodo successivo.

## Invio del token

Per inviare il token viene utilizzato la RPC `sendToken`

# Casi limite

## Due nodi entrano contemporanemente nella rete

Uno nodo, prima di entrare nella rete, rete comunica al gateway le proprie intenzioni.
Supponiamo che A e B siano due nodi che vogliono entrare nella rete e inviano una richiesta simultanea al server. 
Per gestire queste richieste simultanee le porzioni di codice che gestiscono l'inserimento del nodo sono in *sezione
critica*:

```java
public synchronized List<Sensor> add(Sensor s) {
  if(sensorList.contains(s)){
      return null;
  } else {
      sensorList.add(s);
      PushNotificationImpl.add(s);
      return sensorList;
  }
}
```
questo garantisce che le richieste di A e B siano gestite senza sovrapposizioni. Bisogna notare che l'ordine
di computazione non è importante, ciò che è importante è che almeno uno dei due nodi sappia dell'esistenza dell'altro
in modo tale da potersi presentare ad esso.

## Due nodi escono contemporaneamente dalla rete

Sia A-B-C la topologia di rete e B,C i due nodi che stanno uscendo contemporaneamente dalla rete, i casi sono:
* **A possiede il token**. Prova ad inviarlo a B ma non riesce, poi a C e non riesce e infine non lo invia perché non è rimasto
nessun altro a cui inviarlo e quindi continua a produrre misurazioni fin quando B e C non gli comunicano la rimozoine
* **B (o C) possiede il token**. B (o C) spegne il server e lo tiene fin quando non termina l'operazione di rimozione o inserisce 
una misurazione.

Vedere entrambi i casi con sleep dentro `SensorServer.interrupt`.

## Un nodo si sta rimuovendo e un altro sta per inviare il token
Sia A il nodo che deve inviare il token e B il nodo che si sta rimuovendo allora i casi sono i seguenti:
* **B non è il prossimo di A**. Allora non c'è nessun problema ad inviare il token.
* **B è il prossimo di A e ha comunicato A la sua rimozione**. Allora A non invia il token a B ma al successivo ancora.
* **B è il prossimo di A e non ha ancora comunicato la sua rimozione**. Allora A invia il token a B il quale ha interrotto
il proprio server quindi non risponde ed è per questo che A prova con il successivo. (Vedere con sleep dentro `SensorServer.interrupt`)

**N.B** Il terzo punto dipende dall'ordine di terminazione di server o controller, qualora venisse spento il server 
successivamente allora gli arriva il token a B e lo tiene fin quando non termina l'operazione di rimozione o inserisce 
una misurazione.

## Un nodo si sta presentando e un altro sta per inviare il token
Supponiamo di avere la seguente rete A-B-C, e supponiamo che B deve inviare il token al suo successivo (al momento 
C) e proprio in quel momento si inserisce un altro nodo D
1. **D non è il prossimo di B**. Allora fa nulla il token viene inviato correttamente a C.
1. **D è il prossimo di B e ha comunicato il suo inserimento a B**. In tal caso il token viene invato correttamente a D.
1. **D è il prossimo di B e non ha ancora comunicato il suo inserimento a B**. Allora il token non viene inviato ad D ma a C e allora accade la seguente cosa: 
supponiamo che il token conteneva le misurazioni di A e B a quel punto D inserisce la sua misurazione e invia 
il token ad A senza inviare la statistica globale perché i nodi sono 4, A aggiunge la sua misurazione e invia 
la statistica globale al gateway che contiene le misurazioni A,B,C,A quindi si crea una statistica errata
perché vengono prese due misurazioni di A. Comunque ciò non influisce più di tanto perché al prossimo giro le
misurazioni e il passaggio di token si riassesta da solo.

## Un nodo si sta presentando e un altro sta uscendo
Sia A il nodo che sta entrando e B il nodo che sta uscendo allora i casi sono i seguenti:
1. **A ha comunicato la propria entrata a B prima che B uscisse**. In tal caso B sa dell'entrata 
di A e comunica anche a lui la propria uscita.
1. **B è uscito prima che A comunicasse la propria entrata**. Quando A chiederà la lista dei nodi il nodo B 
non sarà più presente.
1. **A ha comunicato la propria entrata al gateway ma non a B**. Accade che B quando esce non lo comunica ad
A che ancora non si è presentato, quindi A si presenterà a B e a seguito di una mancata risposta eliminerà 
B dalla propria lista. (Vedere con sleep dentro `SensorController.sayHelloToOthersNode`)

# Alcune note implementative

Dato che considerati secondari ho trattato i seguenti aspetti nella maniera più semplice possibile:

- per suddividere il codice di ogni componente ho dovuto replicare le classi Java che definivano i modelli 
(e.g. Sensor.java, Measurement.java). Ovviamente si tratta soltanto di un copia e incolla e quindi non c'è nessuna 
dipendenza stretta tra i file ne consegue che qualora io dovessi modificare un file allora dovrei ripetere la stessa 
modifica su tutti quelli replicati. Ovviamente questa si tratta della soluzione più semplice perché non si prevodono 
future modifiche ai file ma non è la soluzione ideale in termini di organizzazione strutturale del codice.
- tutte le operazioni sulla lista dei sensori (che ogni nodo contiene) potrebbero essere rese più efficienti 
se organizzata in una lista ordinata cosa che in questo codice non viene fatta.
